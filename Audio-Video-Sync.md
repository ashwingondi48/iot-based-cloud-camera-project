## AUDIO/VIDEO Syncing for the PI

![](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQizbdiVcV4evucir4fiaRWyjl57SRqUP9pkQ&usqp=CAU)
##### It is the leading multimedia framework, able to decode, encode, transcode, mux, demux, stream, filter and play pretty much anything that humans and machines have created. No matter if they were designed by some standards committee, the community or a corporation.It is also highly portable: FFmpeg compiles, runs, and passes our testing infrastructure FATE across Linux, Mac OS X, Windows,Solaris,etc. right from the command line.
##### FFmpeg can also receive from "a source" (for instance live or UDP) and then transcode and re-broadcast the stream. My task here was to figure out a way so that we can watch video synced with audio both coming from raspberry pi separately.<br><br>
##### So, I figured some steps as follows to accomplish this:
### Method-1:
1. Suppose I have a server running VLC getting stream from the camera, encoding it as MPEG2 stream and sending it to port 5000 through 5003 on an intermediary server. With another server running AWS, with an instance named "live". And this intermediate server will input in MPEG2 stream coming in on ports 5000 sent by raspberry pi cam, to 5003 port transcoding them into mp4 streams with H.264 and AAC codecs and pushing the transcoded stream on to AWS.

2. Now we need to pull the stream from port 5000, transcode it, and push it and for that we can put the following in the **command line**:
`ffmpeg -i 'udp://localhost:5000?fifo_size=1000000&overrun_nonfatal=1' -crf 30 -preset ultrafast -vcodec libx264 -r 25 -b:v 500k -f flv 'rtmp://<AWS server IP>/live/cam0'-i 'udp://localhost:5000?fifo_size=1000000&overrun_nonfatal=1'`
tells ffmpeg where to pull the input stream from. The parts after the ? are probably not needed most of the time.
    - **crf 30** sets the Content Rate Factor. That's an x264 argument that tries to keep reasonably consistent video quality, while varying bitrate during more 'complicated' scenes, etc. A value of 30 allows somewhat lower quality and bit rate.
    - **vcodec** libx264 sets the video codec
    - **r 25** sets the frame rate
    - **b:v 500k** set the video bit rate
    - **f flv** says to deliver the output stream in an flv wrapper
    - **rtmp://<AWS server IP>/live/cam0'** is where the transcoded video stream gets pushed to.
3. Now that we are done streaming video from cam module, we will similarly stream the audio coming from raspberry pi sound card as follows:<br><br>
For example, the following command will generate a signal, and will stream it to the IP Address provided:`ffmpeg -re -f lavfi -i aevalsrc="sin(400*2*PI*t)" -ar 8000 -f mulaw -f rtp rtp://<AWS server IP> `

**Note:** IP address of the video stream and the audio stream be same. So, when the user tries to watch the stream, he hopefully is able to listen to audio along with video stream in real time in sync as USB sound card raspberry pi cam share the same clock.

4. On AWS side we need to create **AWS elemental medialive** with two inputs(Audio & video streams respectively). Well for best step by step process I would like you to watch this video- [LIVE streaming with AWS](https://www.youtube.com/watch?v=Rwvta8_INHY)<br>

**_Come to a closer understanding by looking at the image below_**
![Take a look at this image](https://d1.awsstatic.com/awselemental/v2diagrams/product-page-diagram-Elemental-MediaLive@2x.2570dba1da7763ff116d3dc2897e772ff238b63e.png)
### Method-2:
  1. Capture audio and video stream coming from usb sound card and raspberry cam respectively then combine them using FFmpeg then stream it altogether to a UDP stream.<br> **command line:**`raspivid -o - -t 0 -n -w 1280 -h 720 -fps 25 | ffmpeg -report -probesize 32000 -thread_queue_size 1024 -y -f h264 -r 25 -framerate 25 -i - -thread_queue_size 512 -f alsa -ac 1 -channel_layout mono -i hw:1 -vcodec copy -acodec mp3 -f mpegts udp://44.0.0.95:1234`<br>
  suppose we want to stream it to AWS servers we should give the Aws server IP address where you created the **AWS elemental medialive** with input _(process is mentioned above)_. 
2. Finally, everything is supposed to work well without sync issues. However , if there are any sync issues I would recommend for us to use [**UV4L**](http://www.linux-projects.org/uv4l/) for perfectly synced, low latency (~200ms), Audio/Video streaming to any [**WebRTC**](https://webrtc.org/) compliant peer (e.g. browsers). No configuration is required, just install and connect to the Streaming Server. Since the UV4L back-end is also a V4L2-compliant driver, we can still optionally use ffmpeg if really needed.<br>
You can refer to more info regarding this on [FFmpeg streaming guide.](https://trac.ffmpeg.org/wiki/StreamingGuide)<br>
#### At this point I really hope these ideas work.

**Well I just want to share an additional tool that might work(i.e. Snowmix). I haven't really done much research on it because when we split up work for more efficiency towards final goal, it was supposed to be completed by my teammate.**
#### [Snowmix](https://sourceforge.net/projects/snowmix/)
It is a tool for mixing live and recorded video and audio feeds. Input and outputs can be done through GStreamer pipelines or the GStreamer shmsrc/shmsink API.I haven’t done quite as much research as I did on FFmpeg because it was supposed to be done by my teammate. Anyway, here are some basic features that it can perform:

**Features**
- Audio and Video Mixing - Low Overhead
- Unlimited Number of Audio and Video Feeds
- Unlimited Video Geometry and Frame Rate
- Unlimited Vector Based Image and Text Overlay
- Animation of Images, Texts and Video Feeds
- Advanced support for Cairo Graphics Primitives
- Unlimited Scaling, Rotation, Alpha Blending, Gradients and Masking of Images, Texts and Video
- Low Bandwidth Remote Management and Control
- Scriptable for Complex Operations
- Examples included for mixing Live video to YouTube Live, UStream and LiveStream
- OpenGL hardware accelerated support


_**I will be glad even if my research would help us all a tiny bit, to achieve our IOT Cloud Cam project completion.**_

